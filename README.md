# java

#### 介绍
用spring的aop对redis进行整合并统一管理

#### 软件架构
使用了spring AOP管理，不会侵入原有逻辑；只需在加缓存的方法上面加上注解@InterfaceCacheableDao(key="channeDao",fieldKey="#channelId",expireTime=20)
