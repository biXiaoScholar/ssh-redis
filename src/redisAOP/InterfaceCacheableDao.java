/**
 * @author huanghong
 * @description
 */
package redisAOP;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface InterfaceCacheableDao {
	String key();
	String fieldKey();
	int expireTime() default 3600;
}
