/**
 * @author huanghong
 * @description
 */
package redisAOP;

import java.lang.reflect.Method;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.Assert;

import com.samton.common.redis.bean.RedisCacheBean;
import com.samton.common.redis.dao.InterfaceCacheableDao;

//通知类
@Aspect
public class RedisAdvice {
	/**
	 * 获取注入对象
	 */
	private RedisCacheBean redisCacheBean;
	
	public RedisCacheBean getRedisCacheBean() {
		return redisCacheBean;
	}
	
	public void setRedisCacheBean(RedisCacheBean redisCacheBean) {
		this.redisCacheBean = redisCacheBean;
	}

	@SuppressWarnings("unchecked")
	@Around("@annotation(com.samton.common.redis.dao.InterfaceCacheableDao)")
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		Object result =null;
		InterfaceCacheableDao cacheable=null;
		//获取注解上的完整方法
		Method proxyMethod = ((MethodSignature)pjp.getSignature()).getMethod();
		Method soruceMethod = pjp.getTarget().getClass().getMethod(proxyMethod.getName(), proxyMethod.getParameterTypes());
	
        cacheable=soruceMethod.getAnnotation(com.samton.common.redis.dao.InterfaceCacheableDao.class);
        
        if( null == cacheable ){
        	cacheable = pjp.getTarget().getClass().getAnnotation(com.samton.common.redis.dao.InterfaceCacheableDao.class);
		}
        //解析注解
        String fieldKey =parseKey(cacheable.fieldKey(),soruceMethod,pjp.getArgs());
        
        //获取方法的返回类型,让缓存可以返回正确的类型
        Class returnType=((MethodSignature)pjp.getSignature()).getReturnType();
        
        //使用redis 的hash进行存取，易于管理
        result= redisCacheBean.hget(cacheable.key(), fieldKey,returnType);
        
        if( null == result ){
            try {
                result=pjp.proceed();
                Assert.notNull(fieldKey);
                redisCacheBean.hset(cacheable.key(),fieldKey, result,cacheable.expireTime());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        
        return result;
	}
	
	
	 /**
     *    获取缓存的key 
     *    key 定义在注解上，支持SPEL表达式
     * @param pjp
     * @return
     */
    private String parseKey(String key,Method method,Object [] args){  
    	if(!key.startsWith("#")){
    		return key;
    	}
        //获取被拦截方法参数名列表和参数 用于匹配spel 表达式的值
        LocalVariableTableParameterNameDiscoverer u =   
            new LocalVariableTableParameterNameDiscoverer();  
        String [] paraNameArr=u.getParameterNames(method);
        
        //使用SPEL进行key的解析
        ExpressionParser parser = new SpelExpressionParser(); 
        //SPEL上下文
        StandardEvaluationContext context = new StandardEvaluationContext();
        //把方法参数放入SPEL上下文中
        for(int i=0;i<paraNameArr.length;i++){
            context.setVariable(paraNameArr[i], args[i]);
        }
        
        return parser.parseExpression(key).getValue(context,String.class);
    }
    
}
