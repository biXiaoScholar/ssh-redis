/**
 * @author huanghong
 * @date 2019-4-30 上午11:14:09
 * @description
 */
package redisAOP;

import com.alibaba.fastjson.JSON;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class RedisCacheBean {
	/**
	 * 获取注入对象
	 */
	private JedisPool jedisPool;
	public JedisPool getJedisPool() {
		return jedisPool;
	}
	
	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}
	
	 private Jedis getJedis() {
        int timeoutCount = 0;
        while (true) {
            try {
                if (null != jedisPool) {
                    return jedisPool.getResource();
                }
            } catch (Exception e) {
                if (e instanceof JedisConnectionException) {
                    timeoutCount++;
                    if (timeoutCount > 3) {
                        break;
                    }
                } else {
                    System.out.println("jedisInfo ... NumActive=" + jedisPool.getNumActive()
                            + ", NumIdle=" + jedisPool.getNumIdle()
                            + ", NumWaiters=" + jedisPool.getNumWaiters()
                            + ", isClosed=" + jedisPool.isClosed());
               
                    break;
                }
                
            }
            break;
        }
        return null;
    }
	/**
     * 把对象放入Hash中
     */
    public void hset(String key,String field,Object o,int expireTime){
        Jedis jedis = getJedis();
        
        if (false == jedis.exists(key) ){
        	jedis.hset(key,field, JSON.toJSONString(o));
        	jedis.expire(key, expireTime);
        }else{
        	jedis.hset(key,field, JSON.toJSONString(o));
        }
       
        jedis.close();
    }
    /**
     * 从Hash中获取对象
     */
    public String hget(String key,String field){
        Jedis jedis = getJedis();
        String text=jedis.hget(key,field);
        jedis.close();
        
        return text;
    }
    /**
     * 从Hash中获取对象,转换成制定类型
     */
    public <T> T hget(String key,String field,Class<T> clazz){
        String text=hget(key, field);
        T result=JSON.parseObject(text, clazz);
        
        return result;
    }
    /**
     * 从Hash中删除对象
     */
    public Long hdel(String key,String ... field){
        Jedis jedis = getJedis();
        Long result=jedis.hdel(key,field);
        jedis.close();
        
        return result;
    }
    
}
